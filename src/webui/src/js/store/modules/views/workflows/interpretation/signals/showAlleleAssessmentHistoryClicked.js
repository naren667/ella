import showAlleleAssessmentHistoryModal from '../actions/showAlleleAssessmentHistoryModal'

export default [
    showAlleleAssessmentHistoryModal,
    {
        result: [],
        dismissed: []
    }
]
